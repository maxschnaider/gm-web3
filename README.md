# gm web3 - gangsta motherfucker template for web dapps

<p>
    <img src="https://img.shields.io/static/v1?label=Made%20with&message=VueJS&color=limegreen&style=for-the-badge&logo=vue.js" />
    <img src="https://img.shields.io/badge/Made%20for-Dapps-orange?style=for-the-badge&logo=ethereum" />
</p>

The project is bootstrapped with `vite vue-ts` template for Vite & Vue 3 & TypeScript, `vagmi + ethers + typechain` for web3 logic, `tailwind.css` for styling, `pinia` for state management, `vue-router` for routing, `vue-query + axios` for network requests, `eslint + prettier` for linting and formatting, `vitest` for tests.

## Install

```
git clone https://gitlab.com/maxschnaider/gm-web3.git
cd gm-web3
npm ci
```

## Usage

```
npm run typechain  # to generate contract types
npm run dev  # to run in dev mode
npm run build  # to build Vue SPA app
npm run test  # to run Vitest tests
```

For more scripts check `package.json`.
